***

**THIS REPO HAS BEEN MIGRATED TO GITHUB https://github.com/mariavilaro/OpenCart-2-Hello-World-Module**

***

This is the Hello World Module from the tutorial "From Beginner to Advanced in OpenCart: Module Development", adapted for OpenCart 2

See the original post with instructions here:
http://code.tutsplus.com/tutorials/from-beginner-to-advanced-in-opencart-module-development--cms-21873